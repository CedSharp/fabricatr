A helper script to help you create your files in a Fabric mod project.

## Releases

The project is not yet mature enough to have releases.
If you want to try this project, you should check the [development section](#development).

## Work in Progress

This project is currently in active development and is not yet
ready for production use. Use at your own risk.

## Description

Fabricatr aims to provide different commands to quickly populate your
project with common files.

For example, when creating a block, one usually must create these files:
- Create a static field in a class which will register a `Block`
- Create a static field in a class which will register a `BlockItem`
- Create a model `.json` file to describe how to render the block
- Create a blockstates `.json` file to describe how to apply texture to the model
- Add a texture `.png` file for the block
- Create a recipe `.json` file to craft the block
- Add an entry to the corresponding tool's `.json` file
- Add an entry to the corresponding harvest level's `.json` file
- Create a loot table `.json` file to specify what will drop from the block
- etc...

Fabricatr provides a command that generates all these files quickly and making sure they are properly registered
such that you can simply edit those files without caring about if the file exists or not.

Here is an example usage of that command:

```shell
$ fabricatr make:block ruby_ore -n 'Ruby Ore' -t pickaxe -l iron
```

*The above command will create an ore block called 'Ruby Ore' with the id 'ruby_ore' which can be mined with an iron pickaxe or better*

## Development

### Project Structure

- `src`: the folder where all the dev happens
- `out/dev`: the folder where the compiled *.class* files are stored
- `out/bin`: the folder where the packaged *.jar* file is stored
- `lib`: the folder where external libraries are stored: ex: Google's gson
- `resources`: the folder where non-source files are stored, ex: java templates

### Running the dev version

If you are on Windows, you can use the `run.ps1` script to compile and run the project.
From within a Fabric project, run:

```shell
$ <path to run.ps1> make:block ruby_ore -n 'Ruby Ore' -t pickaxe -l iron
```

## Building

The project is not yet mature to be built.