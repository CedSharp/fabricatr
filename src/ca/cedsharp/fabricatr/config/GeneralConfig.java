package ca.cedsharp.fabricatr.config;

public class GeneralConfig {
    public String mainClass;                    // Optional, defaults to the entrypoint class defined in fabric settings
    public String mainPackage;                  // Optional, defaults to the package of the mainClass
    public String modIdProperty = "MODID";      // The property of the mainClass that contains the mod's unique id
}
