package ca.cedsharp.fabricatr.config;

public class PackagesConfig {
    public String blocks;               // Optional, defaults to a child package of the mainPackage called "blocks"
    public String items;                // Optional, defaults to a child package of the mainPackage called "items"
}
