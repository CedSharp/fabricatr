package ca.cedsharp.fabricatr.config;

public class ItemGroupConfig {
    public boolean enabled = false;             // Whether to generate and use an item group for generated items or not
    public String groupId;                      // The id of the item group, defaults to the mod's unique id
    public String groupName;                    // The name of the item group, defaults to the mod's capitalized name
}
