package ca.cedsharp.fabricatr.config;

public class LoggingConfig {
    public boolean enabled = false;             // Whether to generate logging statements or not
    public String property = "LOGGER";          // The property of the loggingClass which contains a logger
    public String loggingClass;                 // Optional, if not specified, will be the same as mainClass
    public String loggingPackage;               // Optional, if not specified, will be the same as mainPackage
}
