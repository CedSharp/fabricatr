package ca.cedsharp.fabricatr.config;

import java.util.HashMap;

public class FabricSettings {
    public String id;
    public String name;
    public HashMap<String, String[]> entrypoints;

    public transient String entrypointName;
    public transient String entrypointPackage;
    public transient String entrypointClass;
}
