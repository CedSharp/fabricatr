package ca.cedsharp.fabricatr.config;

import com.google.gson.annotations.SerializedName;

public class RegistryConfig {
    @SerializedName("package")
    public String packageName;                  // Optional, defaults to a child package of the mainPackage called "registry"
    public String blocks;                       // Optional, defaults to the mainClass with "Blocks" appended
    public String items;                        // Optional, defaults to the mainClass with "Items" appended
    public String itemGroups;                   // Optional, defaults to the mainClass with "ItemGroups" appended
}
