package ca.cedsharp.fabricatr.config;

import ca.cedsharp.fabricatr.utils.Files;
import ca.cedsharp.fabricatr.utils.Logger;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


import java.io.File;
import java.io.IOException;

public class Config {
    public static transient Config config;
    private static transient boolean searchForConfig = true;
    private static transient final Logger LOGGER = new Logger(Config.class);

    public transient File projectRoot = null;
    public transient File sourceDir = null;
    public transient File entrypointDir = null;
    public transient File assetsDir = null;
    public transient File dataDir = null;
    public transient File tagsDir = null;
    public transient FabricSettings fabric = null;

    public GeneralConfig general = new GeneralConfig();
    public LoggingConfig logging = new LoggingConfig();
    public RegistryConfig registry = new RegistryConfig();
    public ItemGroupConfig itemGroup = new ItemGroupConfig();
    public PackagesConfig packages = new PackagesConfig();

    public static Config getConfig() {
        if (searchForConfig) {
            File projectRoot = findProjectRoot();
            File fabricSettings = findFabricSettings(projectRoot);

            // Read user settings and assign to config, otherwise use defaults
            tryToGetUserConfig(projectRoot);
            // Read fabric settings and assign to config
            injectFabricSettingsIntoConfig(fabricSettings);
            // This is where we play the guessing game and generate missing config values
            assignConfigDefaults();

            // Set some typical directories
            String srcDir = Files.join(projectRoot.getAbsolutePath(), "src", config.fabric.entrypointName);
            String resourceDir = Files.join(srcDir, "resources");
            config.sourceDir = new File(srcDir, "java");
            config.assetsDir = new File(Files.join(resourceDir, "assets", config.fabric.id));
            config.dataDir = new File(Files.join(resourceDir, "data", config.fabric.id));
            config.tagsDir = new File(Files.join(resourceDir, "data", "minecraft", "tags"));

            // Finally, we are done loading configs!
            searchForConfig = false;
            LOGGER.info("Found project: @|yellow " + config.fabric.name + "|@ in @|cyan " + projectRoot.getPath() + "|@");
        }

        return config;
    }

    private static File findProjectRoot() {
        File gradleFile = Files.find("build.gradle");
        if (gradleFile == null || !gradleFile.exists()) return null;
        File projectRoot = gradleFile.getParentFile();
        if (projectRoot == null || !projectRoot.exists()) {
            LOGGER.error("You do not seem to be inside of a Fabric modding project!");
            System.exit(1);
        }
        return projectRoot;
    }

    private static File findFabricSettings(File projectRoot) {
        String fabricSettingsRelativePath = Files.join(projectRoot.getAbsolutePath(), "src", "main", "resources", "fabric.mod.json");
        File fabricSettings = new File(fabricSettingsRelativePath);

        if (!fabricSettings.exists()) {
            LOGGER.error("Could not find @|cyan fabric.mod.json|@ file, please make sure you have a valid Fabric modding project!");
            System.exit(1);
        }

        return fabricSettings;
    }

    private static void tryToGetUserConfig(File projectRoot) {
        File configFile = Files.find("fabricatr.json");

        if (configFile == null || !configFile.exists()) {
            LOGGER.warn("Could not find @|cyan fabricatr.json|@ file, using default settings!");
            config = new Config();
        } else {
            try {
                Gson gson = new GsonBuilder().create();
                config = gson.fromJson(Files.read(configFile), Config.class);
            } catch (IOException e) {
                LOGGER.warn("There was an issue reading @|cyan fabricatr.json|@ file, using default settings!");
                config = new Config();
            }
        }

        config.projectRoot = projectRoot;
    }

    private static void injectFabricSettingsIntoConfig(File fabricSettings) {
        try {
            Gson gson = new GsonBuilder().create();
            config.fabric = gson.fromJson(Files.read(fabricSettings), FabricSettings.class);
        } catch (IOException e) {
            LOGGER.error("There was an issue reading @|cyan fabric.mod.json|@ file, please make sure you have a valid Fabric modding project!");
            System.exit(1);
        }
    }

    private static void storeEntrypoint(String[] entrypointData) {
        String entrypoint = entrypointData[1];
        int dotIndex = entrypoint.lastIndexOf('.');
        config.fabric.entrypointName = entrypointData[0];
        config.fabric.entrypointPackage = entrypoint.substring(0, dotIndex);
        config.fabric.entrypointClass = entrypoint.substring(dotIndex + 1);
    }

    private static void assignConfigDefaults() {
        storeEntrypoint(guessWhatIsTheEntrypoint());

        // General
        if (config.general.mainClass == null) config.general.mainClass = config.fabric.entrypointClass;
        if (config.general.mainPackage == null) config.general.mainPackage = config.fabric.entrypointPackage;
        config.entrypointDir = new File(Files.join(config.projectRoot.getAbsolutePath(), "src",
                config.fabric.entrypointName, config.fabric.entrypointName,
                config.general.mainPackage.replace('.', '/')));

        // Logging
        if (config.logging.loggingClass == null) config.logging.loggingClass = config.general.mainClass;
        if (config.logging.loggingPackage == null) config.logging.loggingPackage = config.general.mainPackage;

        // Registry
        if (config.registry.packageName == null) config.registry.packageName = config.general.mainPackage + ".registry";
        if (config.registry.blocks == null) config.registry.blocks = config.general.mainClass + "Blocks";
        if (config.registry.items == null) config.registry.items = config.general.mainClass + "Items";
        if (config.registry.itemGroups == null) config.registry.itemGroups = config.general.mainClass + "ItemGroups";

        // ItemGroups
        if (config.itemGroup.groupId == null) config.itemGroup.groupId = config.fabric.id;
        if (config.itemGroup.groupName == null) config.itemGroup.groupName = config.fabric.name;

        // Packages
        if (config.packages.blocks == null) config.packages.blocks = config.general.mainPackage + ".blocks";
        if (config.packages.items == null) config.packages.items = config.general.mainPackage + ".items";
    }

    private static String[] guessWhatIsTheEntrypoint() {
        // Try the default main entrypoint
        if (config.fabric.entrypoints.containsKey("main"))
            return new String[] {"main", config.fabric.entrypoints.get("main")[0]};
        // If there is just one entry, return it
        if (config.fabric.entrypoints.size() == 1)
            for (String entrypoint : config.fabric.entrypoints.keySet())
                return new String[] { entrypoint, config.fabric.entrypoints.get(entrypoint)[0]};
        // Try using the mod's id
        String modId = config.fabric.id;
        if (config.fabric.entrypoints.containsKey(modId))
            return new String[] { modId, config.fabric.entrypoints.get(modId)[0] };
        // Try using the mod's name, in lower case, using underscores
        modId = config.fabric.name.toLowerCase().replace(' ', '_');
        if (config.fabric.entrypoints.containsKey(modId))
            return new String[] { modId, config.fabric.entrypoints.get(modId)[0] };
        // Try using the mod's name, in lower case, using hyphens
        modId = config.fabric.name.toLowerCase().replace(' ', '-');
        if (config.fabric.entrypoints.containsKey(modId))
            return new String[] { modId, config.fabric.entrypoints.get(modId)[0] };
        // Failed to find an entrypoint, let the user know
        LOGGER.error("Failed to find @|yellow " + config.fabric.name + "|@'s entrypoint. You can define the entrypoint in @|cyan fabricatr.json|@.");
        System.exit(1);
        return null;
    }

    public void dump() {
        Gson gson = new GsonBuilder()
                .serializeNulls()
                .setPrettyPrinting()
                .create();
        String json = gson.toJson(this);
        System.out.println(json);
    }
}
