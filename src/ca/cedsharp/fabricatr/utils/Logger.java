package ca.cedsharp.fabricatr.utils;

import picocli.CommandLine.Help.Ansi;

public class Logger {
    private static final boolean SHOW_NAME = false;
    private final String name;

    public Logger(String name) {
        this.name = name;
    }

    public Logger(Class<?> cls) {
        this.name = cls.getSimpleName();
    }

    public Logger() {
        this.name = "";
    }

    public void log(String message) {
        System.out.println(Ansi.AUTO.string(message));
    }

    public void prefixed(String prefix, String styles, String message) {
        String named = (name.isEmpty() || !SHOW_NAME) ? " " : " @|faint (" + name + ")|@ ";
        String styled = "@|"+ styles + " [" + prefix + "]|@" + named + message;
        System.out.println(Ansi.AUTO.string(styled));
    }

    public void info(String message) {
        prefixed("INFO", "blue", message);
    }

    public void warn(String message) {
        prefixed("WARN", "yellow", message);
    }

    public void error(String message) {
        prefixed("ERROR", "bold,red", message);
    }

    public void debug(String message) {
        prefixed("DEBUG", "bold,cyan", message);
    }

    public void todo(String message) {
        prefixed("TODO", "bold,magenta,underline", "@|italic " + message + "|@");
    }
}
