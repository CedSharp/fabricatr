package ca.cedsharp.fabricatr.utils;

public class Text {
    /** Converts something like "example" to "Example" */
    public static String capitalize(String text) {
        return text.substring(0, 1).toUpperCase() + text.substring(1);
    }

    /** Converts something like "my_super_string" to "My Super String" */
    public static String prettify(String text) {
        StringBuilder sb = new StringBuilder();
        for (String s : text.split("_"))
            sb.append(" ").append(capitalize(s));
        return sb.substring(1);
    }

    /** Converts something like "my_super_string" to "MySuperString" */
    public static String classify(String text) {
        StringBuilder sb = new StringBuilder();
        for (String s : text.split("_"))
            sb.append(capitalize(s));
        return sb.toString();
    }
}
