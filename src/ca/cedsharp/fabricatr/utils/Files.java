package ca.cedsharp.fabricatr.utils;

import java.io.*;
import java.util.StringJoiner;
import java.util.stream.Collectors;

public class Files {
    public static File find(String filename) {
        File path = new File(System.getProperty("user.dir"));
        while (path != null && path.exists()) {
            File configFile = new File(path.getAbsolutePath() + File.separatorChar + filename);
            if (configFile.exists()) return configFile;
            path = path.getParentFile();
        }
        return null;
    }

    public static String join(String... parts) {
        StringJoiner joiner = new StringJoiner(File.separator);
        for (String part : parts) joiner.add(part);
        return joiner.toString();
    }

    public static String read(File file) throws IOException {
        return java.nio.file.Files.readString(file.toPath());
    }

    public static String readFromResource(String resource) throws IOException {
        ClassLoader cl = ClassLoader.getSystemClassLoader();

        try (InputStream stream = cl.getResourceAsStream(resource)) {
            if (stream == null) return null;
            try (InputStreamReader reader = new InputStreamReader(stream)) {
                BufferedReader buffer = new BufferedReader(reader);
                return buffer.lines().collect(Collectors.joining(System.lineSeparator()));
            }
        }
    }
}