package ca.cedsharp.fabricatr.commands;

import ca.cedsharp.fabricatr.Main;
import ca.cedsharp.fabricatr.config.Config;
import ca.cedsharp.fabricatr.generators.BlockAssets;
import ca.cedsharp.fabricatr.generators.BlockRegistry;
import ca.cedsharp.fabricatr.generators.ItemRegistry;
import ca.cedsharp.fabricatr.generators.LangGenerator;
import ca.cedsharp.fabricatr.utils.Text;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Help.ColorScheme;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

import java.util.concurrent.Callable;

@Command(name="make:block", description="Creates all files required to register a new block in Fabric",
        abbreviateSynopsis = true,
        sortOptions = false,
        synopsisHeading = "Usage:%n      ",
        descriptionHeading = "%nDescription:%n      ",
        parameterListHeading = "%nParameters:%n",
        optionListHeading = "%nOptions:%n",
        footerHeading = "%nExample:%n    ",
        footer = "@|bold fabricatr make:block|@ ruby_ore @|yellow -n|@ \"Ruby Ore\" @|yellow -l|@ iron%n"
)
public class MakeBlock implements Callable<Integer> {
    //#region CommandLine Options

    @Option(names = {"-h", "--help"}, usageHelp = true, description = "Display this help message.")
    private boolean requestHelp;

    @Parameters(index = "0", paramLabel = "block_id", description = "The block's id as referenced throughout the codebase.")
    private String id;

    @Option(names = {"-n", "--name"}, description = "The block's name as shown to the player, in english. If not specified, the block's id will be capitalized and used instead.")
    private String name;

    @Option(names = {"-t", "--tool"}, description = "The tool (@|bold,underline axe|@, @|bold,underline hoe|@, @|bold,underline pickaxe|@, @|bold,underline shovel|@, @|bold,underline sword|@) required to mine this block. Defaults to @|bold,underline pickaxe|@.",
            defaultValue="pickaxe")
    private String tool;

    @Option(names={"-l", "--level"}, description = "The level of the tool (@|bold,underline wood|@, @|bold,underline stone|@, @|bold,underline iron|@, @|bold,underline diamond|@) required to mine this block. Defaults to @|bold,underline stone|@.",
            defaultValue = "stone")
    private String level;

    @Option(names={"-c", "--custom"}, description = "Create a custom block class.")
    private boolean customClass;

    @Option(names={"-r", "--recipe"}, description = "Create a recipe for this block.")
    private boolean hasRecipe;

    @Option(names={"-R", "--recipe-type"}, description = "The type of recipe (@|bold,underline shaped|@, @|bold,underline shapeless|@, @|bold,underline furnace|@, @|bold,underline blast_furnace|@) to create for this block. Defaults to @|bold,underline shaped|@.",
            defaultValue = "shaped")
    private String recipeType;

    //#endregion

    public Integer call() {
        if (requestHelp) {
            ColorScheme colorScheme = Main.createColorScheme();
            CommandLine.usage(this, System.out, colorScheme);
            return 0;
        }

        Config config = Config.getConfig();

//        dump();
//        System.out.println("\n");
//        config.dump();
//        System.out.println("\n\n");

        BlockRegistry blocks = new BlockRegistry();
        ItemRegistry items = new ItemRegistry();
        LangGenerator lang = new LangGenerator();
        BlockAssets assets = new BlockAssets();

        // Register the block
        String blockProperty = customClass ? blocks.addCustomBlock(id) : blocks.addBlock(id);
        items.addBlockItem(id, blockProperty);

        // Add translation
        if (name == null) name = Text.prettify(id);
        lang.addBlock(id, name);
        lang.addItem(id, name);

        // Add json files
        assets.addBasics(id);
        assets.makeMineable(id, tool, level);
        if (hasRecipe) assets.addRecipe(id, recipeType);

        return 0;
    }

    private void dump() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        gson.toJson(this, System.out);
    }
}
