package ca.cedsharp.fabricatr.commands;

import picocli.CommandLine.Command;
import picocli.CommandLine.HelpCommand;

@Command(name = "fabricatr", mixinStandardHelpOptions = true, version = "0.0.1",
        description = "A fabric mod development companion",
        subcommands = {HelpCommand.class, MakeBlock.class},
        sortOptions = false,
        headerHeading = "",
        synopsisHeading = "Usage:%n      ",
        descriptionHeading = "%nDescription:%n      ",
        parameterListHeading = "%nParameters:%n",
        optionListHeading = "%nOptions:%n",
        commandListHeading = "%nCommands:%n",
        footerHeading = "%n"
)
public class Fabricatr {
}