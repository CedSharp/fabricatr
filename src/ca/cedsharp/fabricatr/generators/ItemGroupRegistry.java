package ca.cedsharp.fabricatr.generators;

import ca.cedsharp.fabricatr.utils.Files;
import ca.cedsharp.fabricatr.utils.Logger;

import java.io.File;
import java.io.IOException;

public class ItemGroupRegistry extends TemplateGenerator {
    protected static final Logger LOGGER = new Logger(ItemGroupRegistry.class);

    public boolean isEnabled() {
        return config.itemGroup.enabled;
    }

    public ItemGroupRegistry() {
        super(false);
        if (config.itemGroup.enabled) {
            if (!path.exists())
                new LangGenerator().addItemGroup(config.itemGroup.groupId, config.itemGroup.groupName);
            ensureExists();
        }
    }

    public String getGroupClass() {
        return config.registry.itemGroups;
    }

    public String getGroupProperty() {
        return config.itemGroup.groupName
                .toUpperCase()
                .replace(" ", "_");
    }

    @Override
    protected File getPath() {
        return new File(registryDir.getAbsolutePath(), config.registry.itemGroups + ".java");
    }

    @Override
    protected String getTemplate() {
        try {
            String template = Files.readFromResource("templates/ItemGroup.java");
            if (template == null) {
                LOGGER.error("Missing template file @|cyan " + "templates/Items.java" + "|@, please report this issue on GitLab.");
                System.exit(1);
            }
            return template;
        } catch (IOException e) {
            LOGGER.error("Missing template file @|cyan " + "templates/Items.java" + "|@, please report this issue on GitLab.");
            System.exit(1);
        }
        return "";
    }

    @Override
    protected String processTemplate(String template) {
        return template
                .replace("[[ITEM_GROUP_CLASS]]", getGroupClass())
                .replace("[[ITEM_GROUP]]", getGroupProperty())
                .replace("[[ITEM_GROUP_ID]]", config.itemGroup.groupId)
                .replace("[[LOGGER_DONE]]", LoggerGenerator.log("Registered all item groups!", "info", 2));
    }

    @Override
    protected void afterGeneratingTemplate() {
        LOGGER.info("Generated @|cyan " + path.getName() + "|@");
        LOGGER.todo("Don't forget to call |@@|yellow,italic %s.registerAllItemGroups();|@ @|italic in|@ @|cyan,italic %s.onInitialize()"
                .formatted(config.registry.itemGroups, config.general.mainClass));
    }
}
