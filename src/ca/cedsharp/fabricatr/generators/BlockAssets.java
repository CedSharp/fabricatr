package ca.cedsharp.fabricatr.generators;

import ca.cedsharp.fabricatr.config.Config;
import ca.cedsharp.fabricatr.utils.Files;
import ca.cedsharp.fabricatr.utils.Logger;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.List;

public class BlockAssets {
    private static final Logger LOGGER = new Logger(BlockAssets.class);
    private final Config config;

    private final File blockstates;
    private final File models;
    private final File textures;
    private final File loot_tables;
    private final File recipes;
    private final File mineable;

    public BlockAssets() {
        this.config = Config.getConfig();
        blockstates = new File(config.assetsDir.getAbsolutePath(), "blockstates");
        models = new File(config.assetsDir.getAbsolutePath(), "models");
        textures = new File(Files.join(config.assetsDir.getAbsolutePath(), "textures", "block"));
        loot_tables = new File(Files.join(config.dataDir.getAbsolutePath(), "loot_tables", "blocks"));
        recipes = new File(config.dataDir.getAbsolutePath(), "recipes");
        mineable = new File(Files.join(config.tagsDir.getAbsolutePath(), "blocks", "mineable"));

        // Ensure all directories exist
        for (File file : new File[]{blockstates, models, textures, loot_tables, recipes, mineable})
            ensureExists(file);

        // Some directories require subdirectories
        ensureExists(new File(models.getAbsolutePath(), "block"));
        ensureExists(new File(models.getAbsolutePath(), "item"));
    }

    public void addBlockState(String blockId) {
        File blockstate = new File(blockstates, blockId + ".json");

        if (blockstate.exists()) return;

        try {
            String template = getTemplate("templates/blockstates.json")
                    .replace("[[MOD_ID]]", config.fabric.id)
                    .replace("[[BLOCK_ID]]", blockId);
            java.nio.file.Files.writeString(blockstate.toPath(), template);
        } catch (IOException e) {
            LOGGER.error("Failed to create blockstate: @|cyan " + blockstate.getAbsolutePath() + "|@");
            System.exit(1);
        }

    }

    public void addModels(String blockId) {
        File blockModel = new File(Files.join(models.getAbsolutePath(), "block", blockId + ".json"));
        File itemModel = new File(Files.join(models.getAbsolutePath(), "item", blockId + ".json"));

        try {
            String blockTemplate = getTemplate("templates/blockModel.json")
                    .replace("[[MOD_ID]]", config.fabric.id)
                    .replace("[[BLOCK_ID]]", blockId);
            String itemTemplate = getTemplate("templates/itemModel.json")
                    .replace("[[MOD_ID]]", config.fabric.id)
                    .replace("[[BLOCK_ID]]", blockId);

            java.nio.file.Files.writeString(blockModel.toPath(), blockTemplate);
            java.nio.file.Files.writeString(itemModel.toPath(), itemTemplate);
        } catch (IOException e) {
            LOGGER.error("Failed to create models in: @|cyan " + models.getAbsolutePath() + "|@");
            System.exit(1);
        }

    }

    public void addTexture(String blockId) {
        File texture = new File(textures.getAbsolutePath(), blockId + ".png");
        if (texture.exists()) return;

        ClassLoader cl = ClassLoader.getSystemClassLoader();
        try {
            InputStream stream = cl.getResourceAsStream("images/block.png");
            if (stream == null) {
                LOGGER.error("Failed to copy texture: @|cyan " + texture.getAbsolutePath() + "|@");
                System.exit(1);
            }
            java.nio.file.Files.copy(stream, texture.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            LOGGER.error("Failed to copy texture: @|cyan " + texture.getAbsolutePath() + "|@");
            System.exit(1);
        }
    }

    public void addLootTable(String blockId) {
        File lootTable = new File(loot_tables, blockId + ".json");

        if (lootTable.exists()) return;

        try {
            String template = getTemplate("templates/blockLootTable.json")
                    .replace("[[MOD_ID]]", config.fabric.id)
                    .replace("[[BLOCK_ID]]", blockId);
            java.nio.file.Files.writeString(lootTable.toPath(), template);
        } catch (IOException e) {
            LOGGER.error("Failed to create loot table: @|cyan " + lootTable.getAbsolutePath() + "|@");
            System.exit(1);
        }

    }

    public void addRecipe(String blockId, String recipeType) {
        File recipe = new File(recipes.getAbsolutePath(), blockId + ".json");

        if (recipe.exists()) return;

        String templateName;
        switch (recipeType) {
            case "shaped" -> templateName = "templates/shapedRecipe.json";
            case "shapeless" -> templateName = "templates/shapelessRecipe.json";
            default -> {
                LOGGER.warn("Unknown recipe of type @|bold " + recipeType + "|@, skipping.");
                return;
            }
        }

        try {
            String template = getTemplate(templateName)
                    .replace("[[MOD_ID]]", config.fabric.id)
                    .replace("[[BLOCK_ID]]", blockId);
            java.nio.file.Files.writeString(recipe.toPath(), template);
        } catch (IOException e) {
            LOGGER.error("Failed to create recipe: @|cyan " + recipe.getAbsolutePath() + "|@");
            System.exit(1);
        }

        LOGGER.info("Added a @|bold "+recipeType+"|@ recipe for @|yellow "+blockId+"|@");
    }

    public void addRecipe(String blockId) {
        addRecipe(blockId, "shaped");
    }

    public void makeMineable(String blockId, String tool, String level) {
        List<String> tools = Arrays.asList("pickaxe", "shovel", "axe", "hoe", "sword");
        List<String> levels = Arrays.asList("wood", "stone", "iron", "diamond", "netherite");

        if (!tools.contains(tool)) {
            LOGGER.warn("Unknown tool type @|bold " + tool + "|@, skipping.");
            return;
        }

        if (!levels.contains(level)) {
            LOGGER.warn("Unknown harvest level @|bold " + level + "|@, skipping.");
            return;
        }

        File toolFile = new File(mineable.getAbsolutePath(), tool + ".json");
        File levelFile = new File(mineable.getParentFile().getAbsolutePath(), "needs_" + level + "_tool.json");

        String value = config.fabric.id + ":" + blockId;
        addToArrayJson(toolFile, value);
        addToArrayJson(levelFile, value);

        LOGGER.info("Made @|yellow "+blockId+"|@ mineable with a @|bold "+level+" "+tool+"|@");
    }

    public void addBasics(String blockId) {
        addBlockState(blockId);
        addModels(blockId);
        addTexture(blockId);
        addLootTable(blockId);

        LOGGER.info("Added all basic json files for block @|yellow "+blockId+"|@");
    }

    protected void addToArrayJson(File file, String value) {
        String template = getTemplate("templates/tag.json");

        if (!file.exists()) {
            LOGGER.warn("File @|bold " + file.getAbsolutePath() + "|@ does not exist, creating it...");
            try {
                java.nio.file.Files.writeString(file.toPath(), template
                        .replace("[[VALUES]]", "[]"));
            } catch (IOException e) {
                LOGGER.error("Failed to create file: @|cyan " + file.getAbsolutePath() + "|@");
                System.exit(1);
            }
        }

        try {
            String json = java.nio.file.Files.readString(file.toPath());
            JsonObject obj = new JsonParser().parse(json).getAsJsonObject();
            JsonArray values = obj.getAsJsonArray("values");
            values.add(value);
            java.nio.file.Files.writeString(file.toPath(), template
                    .replace("[[VALUES]]", values.toString()));
        } catch (IOException e) {
            LOGGER.error("Failed to add @|bold " + value + "|@ to @|cyan " + file.getAbsolutePath() + "|@");
            System.exit(1);
        }
    }

    protected void ensureExists(File file) {
        if (file.exists()) return;
        LOGGER.warn("Missing directory @|cyan %s|@, creating it...".formatted(file.getName()));

        if (!file.mkdirs()) {
            LOGGER.error("Failed to create directory: @|cyan " + file.getAbsolutePath() + "|@");
            System.exit(1);
        }
    }

    protected String getTemplate(String templateName) {
        try {
            String template = Files.readFromResource(templateName);
            if (template == null) {
                LOGGER.error("Missing template file @|cyan " + templateName + "|@, please report this issue on GitLab.");
                System.exit(1);
            }
            return template;
        } catch (IOException e) {
            LOGGER.error("Missing template file @|cyan " + templateName + "|@, please report this issue on GitLab.");
            System.exit(1);
        }
        return "";
    }
}
