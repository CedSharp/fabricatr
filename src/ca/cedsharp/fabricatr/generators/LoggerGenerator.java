package ca.cedsharp.fabricatr.generators;

import ca.cedsharp.fabricatr.config.Config;

import java.util.Objects;

public class LoggerGenerator {
    private static final Config config = Config.getConfig();

    private static String getIndent(int indent) {
        return " ".repeat(Math.max(0, indent * 4));
    }

    /** Generates the import statement for the custom logger, otherwise returns an empty string. */
    public static String getImport(int indent) {
        if (!config.logging.enabled) return "";
        return (Objects.equals(config.logging.loggingPackage, config.general.mainPackage)
                && Objects.equals(config.logging.loggingClass, config.general.mainClass))
                ? ""
                : "\n" + getIndent(indent) + "import " + config.logging.loggingPackage +
                    "." + config.logging.loggingClass + ";";
    }

    /** Generates the import statement for the custom logger, otherwise returns an empty string. */
    public static String getImport() {
        return getImport(0);
    }

    /**
     * Generates a logger statement, using custom class if defined.
     * @param message   The message to log. Require quotes to be included!
     * @param method    The method of the logger to use, ex: info, debug, etc.
     * @param indent    The indentation level.
     * @return          The logger statement.
     */
    public static String log(String message, String method, int indent) {
        if (!config.logging.enabled) return "";
        String indentStr = getIndent(indent);
        String cls = config.logging.loggingClass;
        String prop = config.logging.property;
        if (!message.contains("\"")) message = '"' + message + '"';
        return "\n" + indentStr + cls + "." + prop + "." + method + "(" + message + ");";
    }

    /**
     * Generates a logger statement, using custom class if defined.
     * @param message   The message to log. Require quotes to be included!
     * @param method    The method of the logger to use, ex: info, debug, etc.
     * @return          The logger statement.
     */
    public static String log(String message, String method) {
        return log(message, method, 2);
    }

    /**
     * Generates a logger info statement, using custom class if defined.
     * @param message  The message to log. Require quotes to be included!
     * @return         The logger statement.
     */
    public static String log(String message) {
        return log(message, "info", 2);
    }
}
