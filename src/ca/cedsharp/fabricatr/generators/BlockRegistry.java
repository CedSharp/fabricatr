package ca.cedsharp.fabricatr.generators;

import ca.cedsharp.fabricatr.utils.Files;
import ca.cedsharp.fabricatr.utils.Logger;
import ca.cedsharp.fabricatr.utils.Text;

import java.io.File;
import java.io.IOException;

public class BlockRegistry extends TemplateGenerator {
    protected static final Logger LOGGER = new Logger(BlockRegistry.class);

    public String addBlock(String blockId) {
        String template = getTemplate("templates/SimpleBlock.java")
                .replace("[[BLOCK_ID]]", blockId)
                .replace("[[BLOCK_DEF_NAME]]", blockId.toUpperCase());

        try {
            String content = java.nio.file.Files.readString(path.toPath());
            content = insertBefore("public static void registerAllBlocks",
                    "\n" + template + "\n", content);
            java.nio.file.Files.writeString(path.toPath(), content);

            LOGGER.info("Added block @|yellow " + blockId + "|@ to @|cyan " + path.getName() + "|@");
        } catch (IOException e) {
            LOGGER.error("Failed to read @|cyan " + path.getName() + "|@, please report this issue on GitLab.");
            System.exit(1);
        }
        return blockId.toUpperCase();
    }

    public String addCustomBlock(String blockId) {
        generateCustomBlockClass(blockId);

        String template = getTemplate("templates/CustomBlock.java")
                .replace("[[BLOCK_ID]]", blockId)
                .replace("[[BLOCK_DEF_NAME]]", blockId.toUpperCase())
                .replace("[[BLOCK_CLASS]]", Text.classify(blockId));

        try {
            String content = java.nio.file.Files.readString(path.toPath());

            content = insertBefore("public static void registerAllBlocks",
                    "\n" + template + "\n", content);

            if (!config.registry.packageName.equals(config.packages.blocks)) {
                String match = "import " + config.general.mainPackage + "." + config.general.mainClass + ";";
                String insert = "\nimport " + config.packages.blocks + "." + Text.classify(blockId) + ";";
                content = insertAfter(match, insert, content);
            }

            java.nio.file.Files.writeString(path.toPath(), content);

            LOGGER.info("Added block @|yellow " + blockId + "|@ to @|cyan " + path.getName() + "|@");
        } catch (IOException e) {
            LOGGER.error("Failed to read @|cyan " + path.getName() + "|@, please report this issue on GitLab.");
            System.exit(1);
        }

        return blockId.toUpperCase();
    }

    protected void generateCustomBlockClass(String blockId) {
        String blockClass = Text.classify(blockId);
        File blockPackage = new File(config.sourceDir.getAbsolutePath() + "/" + config.packages.blocks.replaceAll("\\.", "/"));
        File blockFile = new File(blockPackage.getAbsolutePath(), blockClass + ".java");

        ensureCustomBlockClassExists(blockClass, blockPackage, blockFile);

        String template = getTemplate("templates/BlockClass.java")
                .replace("[[BLOCKS_PACKAGE]]", config.packages.blocks)
                .replace("[[BLOCK_CLASS]]", blockClass);

        try {
            java.nio.file.Files.writeString(blockFile.toPath(), template);
            LOGGER.info("Added custom block @|yellow " + blockId + "|@ to @|cyan " + blockFile.getName() + "|@");
        } catch (IOException e) {
            LOGGER.error("Failed to write @|cyan " + blockFile.getName() + "|@, please report this issue on GitLab.");
            System.exit(1);
        }
    }

    protected void ensureCustomBlockClassExists(String blockClass, File blockPackage, File blockFile) {
        if (!blockPackage.exists()) {
            LOGGER.warn("Missing package: @|cyan " + config.packages.blocks + "|@, creating it...");
            if (!blockPackage.mkdirs()) {
                LOGGER.error("Unable to create directories!");
                System.exit(1);
            }
        }

        if (!blockFile.exists()) {
            try {
                if (!blockFile.createNewFile()) LOGGER.warn("Unable to create file: @|cyan " + blockFile.getName() + "|@");
            } catch (IOException e) {
                LOGGER.error("Unable to create file!");
                System.exit(1);
            }

            if (!blockFile.canWrite()) {
                LOGGER.error("Unable to write to file!");
                System.exit(1);
            }
        }
    }

    @Override
    protected File getPath() {
        return new File(registryDir.getAbsolutePath(), config.registry.blocks + ".java");
    }

    @Override
    protected String getTemplate() {
        return getTemplate("templates/Blocks.java");
    }

    @Override
    protected String processTemplate(String template) {
        return template
                .replace("[[BLOCKS_CLASS]]", config.registry.blocks)
                .replace("[[LOGGER_DONE]]", LoggerGenerator.log("Registered all blocks!"))
                .replace("[[LOGGER_BLOCK]]", LoggerGenerator.log("\"- Registered block \" + block_id"));
    }

    @Override
    protected void afterGeneratingTemplate() {
        LOGGER.info("Generated @|cyan " + path.getName() + "|@");
        LOGGER.todo("Don't forget to call |@@|yellow,italic %s.registerAllBlocks();|@ @|italic in|@ @|cyan,italic %s.onInitialize()"
                .formatted(config.registry.blocks, config.general.mainClass));
    }
}
