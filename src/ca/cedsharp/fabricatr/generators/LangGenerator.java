package ca.cedsharp.fabricatr.generators;

import ca.cedsharp.fabricatr.config.Config;
import ca.cedsharp.fabricatr.utils.Files;
import ca.cedsharp.fabricatr.utils.Logger;

import java.io.File;
import java.io.IOException;

public class LangGenerator {
    private static final Logger LOGGER = new Logger(LangGenerator.class);
    private static final String TEMPLATE = "{\n}";
    private final Config config;

    public LangGenerator() {
        this.config = Config.getConfig();
    }

    public void add(String key, String value, String lang) {
        String filePath = Files.join(config.assetsDir.getAbsolutePath(), "lang", lang + ".json");
        File file = new File(filePath);
        ensureExists(file);

        try {
            String content = java.nio.file.Files.readString(file.toPath());
            content = append("\"" + key + "\": \"" + value + "\"", content);
            java.nio.file.Files.writeString(file.toPath(), content);
        } catch (IOException e) {
            LOGGER.error("Failed to modify file: @|cyan " + file.getAbsolutePath() + "|@");
            System.exit(1);
        }
    }

    public void add(String key, String value) {
        add(key, value, "en_US");
    }

    public void addBlock(String id, String value, String lang) {
        String key = "block." + config.fabric.id + "." + id;
        add(key, value, lang);
    }

    public void addBlock(String id, String value) {
        String key = "block." + config.fabric.id + "." + id;
        add(key, value);
    }

    public void addItem(String id, String value, String lang) {
        String key = "item." + config.fabric.id + "." + id;
        add(key, value, lang);
    }

    public void addItem(String id, String value) {
        String key = "item." + config.fabric.id + "." + id;
        add(key, value);
    }

    public void addItemGroup(String id, String value, String lang) {
        String key = "itemGroup." + config.fabric.id + "." + id;
        add(key, value, lang);
    }

    public void addItemGroup(String id, String value) {
        String key = "itemGroup." + config.fabric.id + "." + id;
        add(key, value);
    }

    private void ensureExists(File file) {
        if (file.exists()) return;
        LOGGER.warn("Missing lang file @|cyan %s|@, creating it...".formatted(file.getName()));

        try {
            if (!file.getParentFile().mkdirs()) {
                LOGGER.error("Failed to create directory: @|cyan " + file.getParentFile().getAbsolutePath() + "|@");
                System.exit(1);
            }

            if (!file.createNewFile()) {
                LOGGER.error("Failed to create file: @|cyan " + file.getName() + "|@");
                System.exit(1);
            }

            java.nio.file.Files.writeString(file.toPath(), TEMPLATE);
            LOGGER.info("Generated lang file @|cyan %s|@!".formatted(file.getName()));
        } catch (IOException e) {
            LOGGER.error("Failed to create file: @|cyan " + file.getAbsolutePath() + "|@");
            System.exit(1);
        }
    }

    protected String append(String insert, String template) {
        String ending = template.contains("\"") ? ",\n  " : "\n  ";
        return template.replaceFirst("(\\s*})\\s*$", ending + insert + "$1");
    }
}
