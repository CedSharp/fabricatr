package ca.cedsharp.fabricatr.generators;

import ca.cedsharp.fabricatr.utils.Logger;

import java.io.File;
import java.io.IOException;

public class ItemRegistry extends TemplateGenerator {
    protected static final Logger LOGGER = new Logger(ItemRegistry.class);
    protected final ItemGroupRegistry itemGroup;

    public ItemRegistry() {
        super(false);
        itemGroup = new ItemGroupRegistry();
        ensureExists();
    }

    public String addItem(String id) {
        LOGGER.warn("TODO: Add item " + id);
        return "";
    }

    public void addBlockItem(String id, String blockProperty) {
        String itemGroupStr = itemGroup.isEnabled()
                ? ".group(" + itemGroup.getGroupClass() + "." + itemGroup.getGroupProperty() + ")"
                : "";
        String template = getTemplate("templates/BlockItem.java")
                .replace("[[ITEM_DEF_NAME]]", id.toUpperCase())
                .replace("[[ITEM_ID]]", id)
                .replace("[[ITEM_GROUP]]", itemGroupStr)
                .replace("[[BLOCKS_CLASS]]", config.registry.blocks)
                .replace("[[BLOCK_NAME]]", blockProperty);

        try {
            String content = java.nio.file.Files.readString(path.toPath());
            content = insertBefore("public static void registerAllItems",
                    "\n" + template + "\n", content);
            java.nio.file.Files.writeString(path.toPath(), content);

            LOGGER.info("Added block item @|yellow " + id + "|@ to @|cyan " + path.getName() + "|@");
        } catch (IOException e) {
            LOGGER.error("Failed to edit @|cyan " + path.getName() + "|@, please report this issue on GitLab.");
            System.exit(1);
        }
    }

    @Override
    protected File getPath() {
        return new File(registryDir.getAbsolutePath(), config.registry.items + ".java");
    }

    @Override
    protected String getTemplate() {
        return getTemplate("templates/Items.java");
    }

    @Override
    protected String processTemplate(String template) {
        String itemGroupStr = itemGroup.isEnabled()
                ? ".group(" + itemGroup.getGroupClass() + "." + itemGroup.getGroupProperty() + ")"
                : "";
        return template
                .replace("[[ITEMS_CLASS]]", config.registry.items)
                .replace("[[ITEM_GROUP]]", itemGroupStr)
                .replace("[[LOGGER_DONE]]", LoggerGenerator.log("Registered all items!"))
                .replace("[[LOGGER_ITEM]]", LoggerGenerator.log("\"- Registered item \" + item_id"))
                .replace("[[LOGGER_BLOCK]]", LoggerGenerator.log("\"- Registered block item \" + item_id"));
    }

    @Override
    protected void afterGeneratingTemplate() {
        LOGGER.info("Generated @|cyan " + path.getName() + "|@");
        LOGGER.todo("Don't forget to call |@@|yellow,italic %s.registerAllItems();|@ @|italic in|@ @|cyan,italic %s.onInitialize()"
                .formatted(config.registry.items, config.general.mainClass));
    }
}
