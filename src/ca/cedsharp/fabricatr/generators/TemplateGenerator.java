package ca.cedsharp.fabricatr.generators;

import ca.cedsharp.fabricatr.config.Config;
import ca.cedsharp.fabricatr.utils.Files;
import ca.cedsharp.fabricatr.utils.Logger;

import java.io.File;
import java.io.IOException;

public abstract class TemplateGenerator {
    protected static final Logger LOGGER = new Logger(TemplateGenerator.class);
    protected Config config = null;
    protected File registryDir = null;
    protected File path = null;

    public TemplateGenerator(boolean checkExistence) {
        this.config = Config.getConfig();

        String registryRelPath = config.registry.packageName.replace('.', '/');
        registryDir = new File(Files.join(config.sourceDir.getAbsolutePath(), registryRelPath));

        path = getPath();
        if (path == null) {
            LOGGER.error("Template generators should populate the path field!");
            System.exit(1);
        }

        if (checkExistence) ensureExists();
    }

    public TemplateGenerator() {
        this(true);
    }

    protected void ensureExists() {
        if (!registryDir.exists()) {
            LOGGER.warn("Missing package: @|cyan " + config.registry.packageName + "|@, creating it...");
            if (!registryDir.mkdirs()) {
                LOGGER.error("Unable to create directories!");
                System.exit(1);
            }
        }

        if (!path.exists()) {
            LOGGER.warn("Missing file: @|cyan " + path.getName() + "|@, creating it...");

            try {
                if (!path.createNewFile()) LOGGER.warn("Unable to create file: @|cyan " + path.getName() + "|@");
            } catch (IOException e) {
                LOGGER.error("Unable to create file!");
                System.exit(1);
            }

            if (!path.canWrite()) {
                LOGGER.error("Unable to write to file!");
                System.exit(1);
            }

            try {
                String template = getTemplate();
                template = injectBasicData(template);
                template = processTemplate(template);
                java.nio.file.Files.writeString(path.toPath(), template);
                afterGeneratingTemplate();
            } catch (IOException e) {
                LOGGER.error("Unable to write to file!");
                System.exit(1);
            }
        }
    }

    protected abstract File getPath();

    protected abstract String getTemplate();

    protected String getTemplate(String templateName) {
        try {
            String template = Files.readFromResource(templateName);
            if (template == null) {
                LOGGER.error("Missing template file @|cyan " + templateName + "|@, please report this issue on GitLab.");
                System.exit(1);
            }
            return template;
        } catch (IOException e) {
            LOGGER.error("Missing template file @|cyan " + templateName + "|@, please report this issue on GitLab.");
            System.exit(1);
        }
        return "";
    }

    protected String injectBasicData(String template) {
        return template
                .replace("[[PACKAGE]]", config.general.mainPackage)
                .replace("[[REGISTRY_PACKAGE]]", config.registry.packageName)
                .replace("[[ENTRY_CLASS]]", config.general.mainClass)
                .replace("[[MODID]]", config.general.modIdProperty)
                .replace("[[LOGGER_IMPORT]]", LoggerGenerator.getImport());
    }

    protected abstract String processTemplate(String template);

    protected abstract void afterGeneratingTemplate();

    protected String insertBefore(String match, String insert, String template) {
        return template.replaceFirst("(?m)(^\\s*%s.*$)".formatted(match), insert + "$1");
    }

    protected String insertAfter(String match, String insert, String template) {
        return template.replaceFirst("(?m)(^\\s*%s.*$)".formatted(match), "$1" + insert);
    }
}
