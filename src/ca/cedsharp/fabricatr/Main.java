package ca.cedsharp.fabricatr;

import org.fusesource.jansi.AnsiConsole;

import picocli.CommandLine;
import picocli.CommandLine.Help.ColorScheme;
import picocli.CommandLine.Help.Ansi.Style;
import ca.cedsharp.fabricatr.commands.Fabricatr;

public class Main {
    public static ColorScheme createColorScheme() {
        return new ColorScheme.Builder()
                .commands(Style.bold)
                .options(Style.fg_yellow)
                .optionParams(Style.italic)
                .errors(Style.fg_red, Style.bold)
                .stackTraces(Style.fg_red, Style.bold)
                .applySystemProperties()
                .build();
    }

    public static void main(String[] args) {
        boolean isWindows = System.getProperty("os.name").toLowerCase().startsWith("windows");
        if (isWindows) AnsiConsole.systemInstall();

        ColorScheme colorScheme = createColorScheme();
        new CommandLine(new Fabricatr())
                .setColorScheme(colorScheme)
                .setUsageHelpAutoWidth(true)
                .setUsageHelpLongOptionsMaxWidth(40)
                .execute(args);

        if (isWindows) AnsiConsole.systemUninstall();
    }
}
