# Java and Java compiler
$javaDir="C:\Java\jdk-17.0.2\bin"
$java="$javaDir/java.exe"
$javac="$javaDir/javac.exe"

# Dependencies
$depsDir="$HOME/Documents"
$jansi="$depsDir/jansi-2.1.0.jar"
$gson="$depsDir/gson-2.8.1.jar"
#$toml="$depsDir/toml4j-0.7.2.jar"
$jars="$jansi;$gson;"

# Program
$root="$HOME/IdeaProjects/fabricatr"
$src="$root/src"
$dev="$root/out/dev"
$resources="$root/resources"
$main="$root/src/ca/cedsharp/fabricatr/Main.java"
$mainPackage="ca.cedsharp.fabricatr.Main"

# Compile
& $javac -d $dev -sourcepath $src -cp $jars $main

# If no error
if ($?) {
    # Copy resources to destination
    & Copy-Item -Recurse -Force $resources/* $dev
    Clear-Host

    # Run
    & $java -cp "$dev;$jars;" $mainPackage $args
}