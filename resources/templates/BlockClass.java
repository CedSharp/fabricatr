package [[BLOCKS_PACKAGE]];

import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.Block;
import net.minecraft.block.Material;

public class [[BLOCK_CLASS]] extends Block {
    public [[BLOCK_CLASS]]() {
        super(FabricBlockSettings.of(Material.STONE)
            .sounds(BlockSoundGroup.STONE)
            .hardness(4f)
            .strength(1f)
            .requiresTool());
    }
}
