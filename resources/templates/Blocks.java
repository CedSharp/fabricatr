package [[REGISTRY_PACKAGE]];

import net.minecraft.block.Block;
import net.minecraft.block.Material;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;

import [[PACKAGE]].[[ENTRY_CLASS]];[[LOGGER_IMPORT]]

public class [[BLOCKS_CLASS]] {
    public static void registerAllBlocks() {[[LOGGER_DONE]]
    }

    private static Block registerBlock(String name, Block block) {
        Identifier block_id = new Identifier([[ENTRY_CLASS]].[[MODID]], name);[[LOGGER_BLOCK]]
        return Registry.register(Registry.BLOCK, block_id, block);
    }
}