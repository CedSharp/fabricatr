    public static final Block [[BLOCK_DEF_NAME]] =
        registerBlock("[[BLOCK_ID]]", new Block(FabricBlockSettings
            .of(Material.STONE)
            .sounds(BlockSoundGroup.STONE)
            .hardness(4f)
            .strength(1f)
            .requiresTool()));