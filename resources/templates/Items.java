package [[REGISTRY_PACKAGE]];

import net.minecraft.block.Block;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;

import [[PACKAGE]].[[ENTRY_CLASS]];[[LOGGER_IMPORT]]

public class [[ITEMS_CLASS]] {
    public static void registerAllItems() {[[LOGGER_DONE]]
    }

    private static Item registerItem(String name, Item item) {
        Identifier item_id = new Identifier([[ENTRY_CLASS]].[[MODID]], name);[[LOGGER_ITEM]]
        return Registry.register(Registry.ITEM, item_id, item);
    }

    private static BlockItem registerBlockItem(String name, Block block) {
        Identifier item_id = new Identifier([[ENTRY_CLASS]].[[MODID]], name);[[LOGGER_BLOCK]]
        return Registry.register(Registry.ITEM, item_id, new BlockItem(block,
        new FabricItemSettings()[[ITEM_GROUP]]));
    }
}