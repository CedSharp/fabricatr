package [[REGISTRY_PACKAGE]];

import net.fabricmc.fabric.api.client.itemgroup.FabricItemGroupBuilder;
import net.minecraft.block.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;

import [[PACKAGE]].[[ENTRY_CLASS]];[[LOGGER_IMPORT]]

public class [[ITEM_GROUP_CLASS]] {
    public static final ItemGroup [[ITEM_GROUP]] = FabricItemGroupBuilder.build(
        new Identifier([[ENTRY_CLASS]].[[MODID]], "[[ITEM_GROUP_ID]]"),
            // TODO: Change ItemStack to something that represents your mod
            () -> new ItemStack(Item.BLOCK_ITEMS.get(Blocks.COBBLESTONE)));

    public static void registerAllItemGroups() {[[LOGGER_DONE]]
    }
}